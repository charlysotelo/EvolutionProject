﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Creature : MonoBehaviour {

	public Genome genome;

	public LegController leftLeg;
	public LegController rightLeg;

	public FootController leftFoot;
	public FootController rightFoot;

	private Vector3 initialPosition;

	// Use this for initialization
	void Start () {
		initialPosition = transform.GetChild(0).position;
	}

	void Awake() {
		genome.leftLeg.m = Random.Range (-1f, 1f);
		genome.leftLeg.M = Random.Range (-1f, 1f);
		genome.leftLeg.p = Random.Range (0.1f, 2f);
		genome.leftLeg.o = Random.Range (-2f, 2f);

		genome.rightLeg.m = Random.Range (-1f, 1f);
		genome.rightLeg.M = Random.Range (-1f, 1f);
		genome.rightLeg.p = Random.Range (0.1f, 2f);
		genome.rightLeg.o = Random.Range (-2f, 2f);

		genome.leftFoot.m = Random.Range (-1f, 1f);
		genome.leftFoot.M = Random.Range (-1f, 1f);
		genome.leftFoot.p = Random.Range (0.1f, 2f);
		genome.leftFoot.o = Random.Range (-2f, 2f);

		genome.rightFoot.m = Random.Range (-1f, 1f);
		genome.rightFoot.M = Random.Range (-1f, 1f);
		genome.rightFoot.p = Random.Range (0.1f, 2f);
		genome.rightFoot.o = Random.Range (-2f, 2f);
	}
	
	// Update is called once per frame
	void Update () {
		leftLeg.position = genome.leftLeg.EvaluateAt(Time.time);
		rightLeg.position = genome.rightLeg.EvaluateAt(Time.time);

		leftFoot.position = genome.leftFoot.EvaluateAt(Time.time);
		rightFoot.position = genome.rightFoot.EvaluateAt(Time.time);
	}

	public float GetScore() {
		return Mathf.Abs(transform.GetChild(0).position.x - initialPosition.x);
	}
}
